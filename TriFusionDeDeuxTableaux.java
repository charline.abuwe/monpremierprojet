/**
 * 
 */
package triFusion;

/**
 * @author Charline
 *
 */
public class TriFusionDeDeuxTableaux {

    /**
     * @param args
     */
    public static void main(final String[] args) {
        double tab[] = { 5, 7, 12, 51, 92 };
        double tab1[] = { 8, 18, 22,44,88 };
        double tab2[] = new double[tab.length + tab1.length];
        int i = 0, j = 0, k = 0;
        for (int t = 0; t < tab.length; t++) {
            System.out.print(tab[t] + " ## ");
        }
        System.out.println("***");
        for (int t = 0; t < tab1.length; t++) {
            System.out.print(tab1[t] + " ## ");
        }
        System.out.println("***");
        while (k < (tab.length + tab1.length)) {
            if (i < tab.length && j < tab1.length) {
                if (tab[i] <= tab1[j]) {
                    tab2[k] = tab[i];
                    System.out.print(tab2[k] + "--");
                    k += 1;
                    i += 1;
                } else {
                    tab2[k] = tab1[j];
                    System.out.print(tab2[k] + "--");
                    k += 1;
                    j += 1;
                }
            } else {
                if (i >= tab.length) {
                    tab2[k] = tab1[j];
                    System.out.print(tab2[k] + "--");
                    k += 1;
                    j += 1;
                } else if (j >= tab1.length) {
                    tab2[k] = tab[i];
                    System.out.print(tab2[k] + "--");
                    k += 1;
                    i += 1;
                }
            }
        }
    }

}
